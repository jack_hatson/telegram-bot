const TelegramBot = require('node-telegram-bot-api');
const { listEvents } = require('./eventsService');
const { buildMessage } = require('./utils/messageUtils');

const token = process.env.TG_TOKEN;

const bot = new TelegramBot(token, { polling: true });

bot.onText(/\/week/, async (msg) => {
  const chatId = msg.chat.id;

  await sendMessage(chatId, 'week');
});

bot.onText(/\/today/, async (msg) => {
  const chatId = msg.chat.id;

  await sendMessage(chatId, 'today');
});


const sendMessage = async (chatId, range) => {
  const result = await listEvents(range);

  await bot.sendMessage(chatId, buildMessage(result, range), { parse_mode: 'HTML',  });
}


module.exports = {
  bot
}