const {google} = require("googleapis");
const { authorize } = require("./authService");
const { setDateRange } = require("./utils/dateUtils");


const listEvents = async (range) => {
  const auth = await authorize();
  const calendar = google.calendar({version: 'v3', auth});
  const { startDate, endDate } = setDateRange(range);
  try {
    const result = await calendar.events.list({
      calendarId: 'testuser251453@gmail.com',
      maxResults: 10,
      singleEvents: true,
      orderBy: 'startTime',
      timeMin: startDate,
      timeMax: endDate,
    });

    const events = result.data.items;

    return events;
  } catch (e) {
    console.error(e);
  }

};

module.exports = {
  listEvents
};