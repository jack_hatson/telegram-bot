const setDateRange = (range) => {
  const startDate = new Date(new Date().setHours(0));
  let endDate;

  switch (range) {
    case 'today':
      endDate = new Date(new Date().setHours(23, 59, 0, 0));
      break;
    case 'week':
      const dayDiff = 7 - new Date().getDay();
      const endDateWithDiff = new Date(new Date().setDate(new Date().getDate() + dayDiff));
      endDate = new Date(endDateWithDiff.setHours(23, 59, 0, 0));
      break;
    default:
      endDate = startDate;
  }

  return { startDate, endDate }
};

module.exports = {
  setDateRange,
};