const buildMessage = (events, range) => {
  const periodStr = range === 'today' ? 'На сьогодні' : 'До кінця тижня';
  let message = `${periodStr} в тебе заплановані такі події: \n\n`;

  if (range === 'week') {
    const groupedEvents = events.reduce((acc, event) => {
      const eventDate = new Date(event.start.dateTime).toLocaleDateString();
      if (!Object.keys(acc).find(item => item === eventDate)) {
        acc[eventDate] = [event];
      } else {
        acc[eventDate].push(event);
      }
      return acc;
    }, {});

    Object.entries(groupedEvents).forEach(([eventDate, events]) => {
      message += `<b>${eventDate}</b>\n`;

      events.forEach(event => {
        message += updateEventMessage(event);
      });

      message += `--------------------------\n\n`;
    })
  } else {
    events.forEach(event => {
      message += updateEventMessage(event);
    });
  }

  return message;
};

const updateEventMessage = (event) => {
    let message =`<a href="${event.htmlLink}">Назва: ${event.summary}</a>\n`;

    if (event.description) {
      message += `Опис: ${event.description}\n`;
    };

    message += `Час події: ${new Date(event.start.dateTime).toLocaleString()}\n\n`;
  return `${message}`;
};

module.exports = { buildMessage }